### LINUX
CC := gcc
LD := gcc

### QNX
#CC := qcc -V gcc_ntoaarch64le
#LD := qcc -V gcc_ntoaarch64le

LD_FLAGS := -lm


objects := $(patsubst %.c,out/%.o,$(wildcard src/*.c))
out_dirs := out out/src

out/sin_fpu: $(out_dirs) $(objects)
	$(LD) $(objects) $(LD_FLAGS) -o $@

out/src/%.o: src/%.c
	$(CC) -c $< -o $@

clean:
	rm -f out/sin_fpu out/src/*

$(out_dirs): 
	mkdir $(out_dirs)
