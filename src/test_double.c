// (c) Sylwester Kocjan 2020

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

struct testData_t
{
    double arg;
    double expValue;
};

struct testData_t sinTestData [] =
{
    { 3.1405926535897932e+000, 9.999998333333801292906e-004},  //pi - 1e-3
    { 3.1415926525897930e+000, 1.000000238462643383112e-009},  //pi - 1e-9
    { 3.1415926535897922e+000, 1.038462643383279502884e-015},  //pi - 1e-15
    { 3.1415926535897931e+000, 0.0e+000},                      //pi
    { 9.4237779607693799e+000, 9.999998333331570546889e-004},  //3*pi - 1e-3
    { 9.4247779597693793e+000, 1.000000415387930149671e-009},  //3*pi - 1e-9
    { 9.4247779607693776e+000, 2.115387930149838508652e-015},  //3*pi - 1e-15
    { 9.4247779607693793e+000, 0.0e+000},                      //3*pi
    { 31.414926535897930e+000,-9.999998333357262919079e-004},  //10*pi - 1e-3
    { 31.415926534897931e+000,-1.000001384626433832628e-009},  //10*pi - 1e-9
    { 31.415926535897921e+000,-1.138462643383279502884e-014},  //10*pi - 1e-14
    { 31.415926535897931e+000, 0.0e+000},                      //10*pi
    { 31415.925535897932e+000,-9.999998337179679081860e-004},  //10000*pi - 1e-3
    { 31415.926535896931e+000,-1.001384626433832794861e-009},  //10000*pi - 1e-9
    { 31415.926535897921e+000,-1.138462643383279502884e-011},  //10000*pi - 1e-12
    { 31415.926535897932e+000, 0.0e+000}                       //10000*pi
};


uint64_t Validate( double actual, double expected )
{
    uint64_t* ptr = (uint64_t*) &actual;
    uint64_t retval = *ptr;

    ptr = (uint64_t*) &expected;
    uint64_t x = *ptr;

    retval -= x;

    return abs(retval);
}


void RunTestsDouble()
{
    const int noOfTestSteps = sizeof(sinTestData) / sizeof(struct testData_t);

    printf("Running double test... ");

    for( int i = 0; i < noOfTestSteps; i++)
    {
        double actual = sin( sinTestData[i].arg );
        uint64_t ulp_diff = Validate( actual, sinTestData[i].expValue );

#ifdef WIN32
        printf("\nStep %d, sin(%f), ULP difference: %I64u",
#else
        printf("\nStep %d, sin(%f), ULP difference: %ld",
#endif
               i, sinTestData[i].arg, ulp_diff );
    }

    printf("\ndone.");
}
